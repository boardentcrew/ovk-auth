//Express
var config = {
	port:8080,
	cookieDomain:'.openvkey.com',
}
var port = 8080;
var cookieDomain = '.openvkey.com';
var express = require('express');
var app = module.exports = express();
var cookieParser = require('cookie-parser');
app.use( cookieParser('secret') );
config.app = app;
var routes = require('./routes.js')(config);
app.listen( config.port );
console.log('Server listening on port',config.port);
