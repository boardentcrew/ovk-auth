var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/ovk');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function(callback) {
        console.log('DB Connection established.');
	var schemas = require('./schemas.js')(mongoose);
	console.log('DB Schemas loaded.');
});
var swig = require('swig');

module.exports = function(config) {
	app = config.app;
	app.get('/', function(req,res) {
		console.log(req.cookies);	
		if (!(req.cookies === undefined) && req.cookies.ovk) {
			var tpl = swig.compileFile('swigs/authed.swig');
			res.send(tpl({'USERNAME':req.cookies.ovk}));
		} else {
			var tpl = swig.compileFile('swigs/not-authed.swig');
			res.send(tpl());
		}
	});
	app.get('/unset', function(req,res) {
		res.clearCookie('ovk', {domain:config.cookieDomain});
		res.send('unset');
	});
	app.get('/set', function(req,res) {
		res.cookie('ovk', 'abc', { domain:config.cookieDomain});
		res.send('set');
	});
}
